import ProtectedApi from '@/api/ProtectedApi';

export interface SignInBody {
	login: string,
	password: string
}

export interface RegisterBody {
	login: string,
	password: string,
	email: string
}

export interface BuySubscriptionBody {
	typeId: number,
	isItFree: number
}

export interface RateFilmBody {
	filmId: number,
	mark: number
}

class MainProtectedApi extends ProtectedApi {
	private static instanceCached: MainProtectedApi;

	private constructor() {
		super('http://localhost:3001/api');
	}

	static getInstance = () => {
		if (!MainProtectedApi.instanceCached) MainProtectedApi.instanceCached = new MainProtectedApi();

		return MainProtectedApi.instanceCached;
	};

	public signInUser = ({ login, password }: SignInBody) => {
		return this.instance.post('/signIn', {
			login,
			password
		})
	};

	public registerUser = ({ login, password, email }: RegisterBody) => {
		return this.instance.post('/signUp', {
			login,
			password,
			email
		})
	};

	public loginViaToken = () => {
		return this.instance.post('/autoLogin');
	};

	public getPaymentsHistory = () => {
		return this.instance.post('/getPayments');
	};

	public buySubscription = ({ typeId, isItFree }: BuySubscriptionBody) => {
		return this.instance.post('/buySubscription', { typeId, isItFree });
	};

	public refreshTokens = (refreshToken: string) => {
		return this.instance.post('/refreshTokens', { refreshToken });
	};

	public rateFilm = ({ filmId, mark }: RateFilmBody) => {
		return this.instance.post('/rateFilm', { filmId, mark });
	};

	public getMarks = () => {
		return this.instance.get('/getMarks');
	};

	public getSubscriptionExpires = () => {
		return this.instance.get('/getSubscriptionExpires');
	};
}

export default MainProtectedApi;