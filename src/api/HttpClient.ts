import axios, { AxiosInstance, AxiosResponse } from 'axios';

export abstract class HttpClient {
	protected readonly instance: AxiosInstance;

	public constructor(baseURL: string) {
		this.instance = axios.create({
			baseURL,
			headers: {
				'Content-Type': 'application/json',
				"Access-Control-Allow-Origin": "http://localhost:3000/"
			}
		});
	}

	protected responseInterceptorsFulfilledHandler = (response: AxiosResponse) => {
		return response.data;
	};
}

export default HttpClient;