import HttpClient from '@/api/HttpClient';

export interface GetFilmsBody {
	genre: string,
	page: string
}

class MainApi extends HttpClient {
	private static instanceCached: MainApi;

	private constructor() {
		super(process.env.BASE_URL);

		this.initializeResponseInterceptors();
	}

	static getInstance = () => {
		if (!MainApi.instanceCached) MainApi.instanceCached = new MainApi();

		return MainApi.instanceCached;
	};

	private initializeResponseInterceptors = () => {
		this.instance.interceptors.response.use(
			this.responseInterceptorsFulfilledHandler
		)
	};

	public getSubscriptionTypes = () => {
		return this.instance.get('/getSubscriptionTypes');
	};

	public getGenres = () => {
		return this.instance.get('/getGenres');
	};

	public getFilms = ({ genre, page }: GetFilmsBody) => {
		return this.instance.get(`/getFilms/${ genre }/${ page }`);
	};
}

export default MainApi;