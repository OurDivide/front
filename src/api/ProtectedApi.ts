import { AxiosError, AxiosRequestConfig } from 'axios';

import HttpClient from '@/api/HttpClient';
import TokensLocalStorage from '@/local-storage/TokensLocalStorage';
import store from "@/redux/store";
import { authActions } from "@/redux/actions/authActions";
import { push } from "connected-react-router";
import { errorActions } from "@/redux/actions/errorActions";
import { subscriptionActions } from "@/redux/actions/subscriptionActions";

export default abstract class HttpClientProtected extends HttpClient {
	protected constructor(baseURL: string) {
		super(baseURL);

		this.initializeRequestInterceptor();
		this.initializeResponseInterceptor();
	}

	private initializeRequestInterceptor = () => {
		this.instance.interceptors.request.use(this.handleRequest);
	};

	private initializeResponseInterceptor = () => {
		this.instance.interceptors.response.use(
			this.responseInterceptorsFulfilledHandler,
			this.responseInterceptorsRejectedHandler);
	};

	private handleRequest = (config: AxiosRequestConfig) => {
		const storage = TokensLocalStorage.getInstance();
		const token = storage.getAccessToken();

		config.headers.Authorization = `Bearer ${token}`;

		return config;
	};

	private responseInterceptorsRejectedHandler = ((error: AxiosError) => {
		const originalRequest = error.config;

		if (error.response && error.response.status === 401) {
			const storage = TokensLocalStorage.getInstance();
			const refreshToken = storage.getRefreshToken();
			const { url, method, data } = originalRequest;

			if (refreshToken) {
				const token = localStorage.getItem('token')!;

				return this.instance.post('/refreshTokens', { refreshToken })
					.then(result => {
						const { token: updatedToken, refreshToken: updatedRefreshToken } = result.data.data;

						storage.setAccessToken(updatedToken);
						storage.setRefreshToken(updatedRefreshToken);
						store.dispatch(authActions.authSuccessful(updatedToken, token));

						if (url !== '/autoLogin') return this.instance({ url, method, data });
						else store.dispatch(push('/films'));
					})
					.catch(reason => {
						store.dispatch(errorActions.receiveError(reason));
					})
			} else {
				storage.clear();

				store.dispatch(authActions.clearState());
				store.dispatch(subscriptionActions.clearState());
				store.dispatch(push('/auth'));
			}
		} else {
			store.dispatch(errorActions.receiveError(error.response!.data.data.message));

			throw new Error(error.response!.data.data.message);
		}
	});
}