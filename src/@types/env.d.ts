declare namespace NodeJS {
    export interface ProcessEnv {
        MODE: 'development' | 'production',
        PORT?: string,
        BASE_URL: string
    }
}