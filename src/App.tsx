import React, { useEffect, useState, Suspense } from 'react';
import { Switch, Route } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { push } from "connected-react-router";

import { autoLogin } from "@/redux/actions/authActions";
import { selectToken, selectPreviousToken } from "@/redux/selectors/authSelector";
import { selectError } from "@/redux/selectors/errorSelector";
import Header from "@/components/Header/Header";
import Loader from "@/components/UI/Loader/Loader";

const Auth = React.lazy(() => import('@/components/Auth/Auth'));
const Profile = React.lazy(() => import('@/components/Profile/Profile'));
const Films = React.lazy(() => import('@/components/Films/Films'));
const Logout = React.lazy(() => import('@/components/Logout/Logout'));
const WatchingFilm = React.lazy(() => import('@/components/WatchingFilm/WatchingFilm'));
const ModalError = React.lazy(() => import("@/components/UI/ModalError/ModalError"));

const App: React.FC = () => {
	const [isAutoLoginDispatched, setIsAutoLoginDispatched] = useState(false);
	const dispatch = useDispatch();
	const token = useSelector(selectToken);
	const previousToken = useSelector(selectPreviousToken);
	const error = useSelector(selectError);

	useEffect(() => {
		if (!token && !isAutoLoginDispatched) {
			setIsAutoLoginDispatched(true);

			dispatch(autoLogin());
		}
		if (!token && isAutoLoginDispatched) dispatch(push('/auth'));
		if (token && previousToken === '') dispatch(push('/films'));
	}, [token, dispatch, isAutoLoginDispatched]);

	return (
		<AppStyled>
			<Suspense fallback={ <Loader/> }>
				{ error ? <ModalError message={ error }/> : null }
			</Suspense>
			<Header/>
			<Content>
				<Suspense fallback={ <Loader/> }>
					<Switch>
						{ !token ? <Route path='/auth' render={ () => <Auth/> }/> : null }
						{ token ? <Route path='/profile' render={ () => <Profile/> }/> : null }
						{ token ? <Route path='/logout' render={ () => <Logout/> }/> : null }
						<Route path='/films' render={ () => <Films/> }/>
						<Route exact path='/watch/:id' render={ () => <WatchingFilm/> }/>
					</Switch>
				</Suspense>
			</Content>
		</AppStyled>
	)
};

const AppStyled = styled.div`
	position: relative;
	display: flex;
  flex-direction: column;
  height: 100%;
`;

const Content = styled.div`
	position: relative;
	padding-top: 80px;
	height: 100%;
`;

export default App;
