import { createSelector, Selector } from 'reselect';

import { State } from "@/redux/store";

const selectErrorState = (state: State) => state.error;

export const selectError: Selector<State, string> = createSelector(
	selectErrorState,
	({ errorMessage }) => errorMessage
);