import { createSelector, Selector } from 'reselect';

import { State } from "@/redux/store";
import Link from "@/interfaces/NavigationLink";
import Film from "@/interfaces/Film";
import Mark from "@/interfaces/Mark";

const selectFilmsState = (state: State) => state.films;

export const selectGenres: Selector<State, string[]> = createSelector(
	selectFilmsState,
	({ genres }) => genres
);

export const selectLinks: Selector<State, Link[]> = createSelector(
	selectFilmsState,
	({ links }) => links
);

export const selectFilms: Selector<State, Film[]> = createSelector(
	selectFilmsState,
	({ films }) => films
);

export const selectPending: Selector<State, boolean> = createSelector(
	selectFilmsState,
	({ pending }) => pending
);

export const selectMarks: Selector<State, Mark[]> = createSelector(
	selectFilmsState,
	({ marks }) => marks
);

export const selectPagesCount: Selector<State, number> = createSelector(
	selectFilmsState,
	({ pagesCount }) => pagesCount
);
