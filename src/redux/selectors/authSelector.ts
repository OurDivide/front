import { createSelector, Selector } from 'reselect';

import { State } from "@/redux/store";

const selectAuth = (state: State) => state.auth;

export const selectToken: Selector<State, string> = createSelector(
	selectAuth,
	({ token }) => token
);

export const selectPreviousToken: Selector<State, string> = createSelector(
	selectAuth,
	({ previousToken }) => previousToken
);
