import { createSelector, Selector } from 'reselect';

import Payments from "@/interfaces/Payments";
import SubscriptionType from "@/interfaces/SubscriptionType";
import { State } from "@/redux/store";

const selectSubscription = (state: State) => state.subscription;

export const selectPayments: Selector<State, Payments[]> = createSelector(
	selectSubscription,
	({ payments }) => payments
);

export const selectExpires: Selector<State, string> = createSelector(
	selectSubscription,
	({ expires }) => expires
);

export const selectPending: Selector<State, boolean> = createSelector(
	selectSubscription,
	({ pending }) => pending
);

export const selectTypes: Selector<State, SubscriptionType[]> = createSelector(
	selectSubscription,
	({ types }) => types
);

export const selectWasFreeUsed: Selector<State, number> = createSelector(
	selectSubscription,
	({ wasFreeUsed }) => wasFreeUsed
);
