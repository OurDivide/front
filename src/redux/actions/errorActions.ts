import { createActionCreators } from "immer-reducer";
import { ErrorReducer } from "@/redux/reducers/errorReducer";

export const errorActions = createActionCreators(ErrorReducer);

export type ErrorActions =
	ReturnType<typeof errorActions.receiveError> |
	ReturnType<typeof errorActions.removeError>;