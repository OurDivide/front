import { createActionCreators } from "immer-reducer";
import { push } from "connected-react-router";

import { AuthReducer } from "@/redux/reducers/authReducer";
import { AsyncAction } from "@/redux/actions/common";
import { subscriptionActions } from "@/redux/actions/subscriptionActions";
import { filmActions } from "@/redux/actions/filmActions";
import { errorActions } from "@/redux/actions/errorActions";
import { SignInBody } from "@/api/MainProtectedApi";
import TokensLocalStorage from '@/local-storage/TokensLocalStorage';

export const authActions = createActionCreators(AuthReducer);

export type AuthActions =
	ReturnType<typeof authActions.authSuccessful> |
	ReturnType<typeof authActions.pending> |
	ReturnType<typeof authActions.clearState>;

interface AuthProps extends SignInBody{
	email?: string
}

export const authenticate = ({ login, password, email }: AuthProps): AsyncAction => async (
	dispatch,
	_,
	{ protectedApi }
) => {
	try {
		dispatch(authActions.pending());

		const response = email ?
			await protectedApi.registerUser({login, password, email}) :
			await protectedApi.signInUser({ login, password });

		if (response && response.data) {
			if (response.data.token) {
				const storage = TokensLocalStorage.getInstance();

				const token = response.data.token!,
					refreshToken = response.data.refreshToken!;

				storage.setAccessToken(token);
				storage.setRefreshToken(refreshToken);

				dispatch(authActions.authSuccessful(token));
			}
		}
	} catch (e) {
		console.log(e);

		dispatch(authActions.pending(false));
	}
};

export const autoLogin = (): AsyncAction => async (
	dispatch,
	_,
	{ protectedApi }
) => {
	try {
		dispatch(authActions.pending());

		const storage = TokensLocalStorage.getInstance();
		const token = storage.getAccessToken();

		if (token) {
			const result = await protectedApi.loginViaToken();

			if (result && result.status === 200) {
				dispatch(authActions.authSuccessful(token));
				dispatch(push('/films'));
			}
		} else {
			const refreshToken = storage.getRefreshToken();

			if (refreshToken) {
				const response = await protectedApi.refreshTokens(refreshToken);

				if (response.data && response.data.token) {
					storage.setAccessToken(response.data.token);
					storage.setRefreshToken(response.data.refreshToken);
				}
			} else dispatch(push('/auth'));
		}
	} catch (e) {
		console.log(e);

		dispatch(authActions.pending(false));
	}
};

export const logout = (): AsyncAction => async (
	dispatch,
	_,
	{ mainApi }
) => {
	const storage = TokensLocalStorage.getInstance();

	storage.clear();

	dispatch(authActions.clearState());
	dispatch(subscriptionActions.clearState());
	dispatch(filmActions.clearState());
	dispatch(errorActions.removeError());
};

