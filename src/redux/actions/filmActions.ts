import { createActionCreators } from "immer-reducer";

import { FilmsReducer } from "@/redux/reducers/filmsReducer";
import { AsyncAction } from "@/redux/actions/common";
import { GetFilmsBody } from "@/api/MainApi";
import { RateFilmBody } from "@/api/MainProtectedApi";

export const filmActions = createActionCreators(FilmsReducer);

export type FilmActions =
	ReturnType<typeof filmActions.receiveGenres> |
	ReturnType<typeof filmActions.receiveFilms> |
	ReturnType<typeof filmActions.pending> |
	ReturnType<typeof filmActions.updateFilmMark> |
	ReturnType<typeof filmActions.receiveUserMarks> |
	ReturnType<typeof filmActions.clearState>;

export const getGenres = (): AsyncAction => async (
	dispatch,
	_,
	{ mainApi }
) => {
	try {
		const response = await mainApi.getGenres();

		if (response.data && response.data.genres) {
			dispatch(filmActions.receiveGenres(response.data.genres));
		}
	} catch (e) {
		console.log(e);

		dispatch(filmActions.pending(false));
	}
};

export const getFilms = ({ genre, page }: GetFilmsBody): AsyncAction => async (
	dispatch,
	_,
	{ mainApi }
) => {
	try {
		dispatch(filmActions.pending());

		const response = await mainApi.getFilms({ genre, page });

		if (response.data && response.data.films) {
			dispatch(filmActions.receiveFilms(response.data.films, response.data.pagesCount));
		}
	} catch (e) {
		console.log(e);

		dispatch(filmActions.pending(false));
	}
};

export const rateFilm = ({ filmId, mark }: RateFilmBody): AsyncAction => async (
	dispatch,
	_,
	{ protectedApi }
) => {
	try {
		const response = await protectedApi.rateFilm({ filmId, mark });

		dispatch(filmActions.updateFilmMark(filmId, response.data.mark));
		dispatch(filmActions.receiveUserMarks([{ mediaId: filmId, mark }]));
	} catch (e) {
		console.log(e);

		dispatch(filmActions.pending(false));
	}
};

export const getMarks = (): AsyncAction => async (
	dispatch,
	_,
	{ protectedApi }
) => {
	try {
		const response = await protectedApi.getMarks();

		dispatch(filmActions.receiveUserMarks(response.data.marks));
	} catch (e) {
		console.log(e);

		dispatch(filmActions.pending(false));
	}
};

