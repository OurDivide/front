import { createActionCreators } from "immer-reducer";

import { SubscriptionReducer } from '@/redux/reducers/subscriptionReducer';
import { AsyncAction } from "@/redux/actions/common";
import { errorActions } from "@/redux/actions/errorActions";
import { BuySubscriptionBody } from "@/api/MainProtectedApi";

export const subscriptionActions = createActionCreators(SubscriptionReducer);

export type SubscriptionActions =
	ReturnType<typeof subscriptionActions.pending> |
	ReturnType<typeof subscriptionActions.receiveHistory> |
	ReturnType<typeof subscriptionActions.receiveTypes> |
	ReturnType<typeof subscriptionActions.receiveSubscription> |
	ReturnType<typeof subscriptionActions.receiveWasFreeUsed> |
	ReturnType<typeof subscriptionActions.clearState>;

export const getPaymentsHistory = (): AsyncAction => async (
	dispatch,
	_,
	{ protectedApi }
) => {
	try {
		dispatch(subscriptionActions.pending());

		const response = await protectedApi.getPaymentsHistory();

		dispatch(subscriptionActions.receiveHistory(response.data.payments));
	} catch (e) {
		dispatch(subscriptionActions.pending(false));
		dispatch(errorActions.receiveError(e));
	}
};

export const getSubscriptionTypes = (): AsyncAction => async (
	dispatch,
	_,
	{ mainApi }
) => {
	try {
		dispatch(subscriptionActions.pending());

		const response = await mainApi.getSubscriptionTypes();

		dispatch(subscriptionActions.receiveTypes(response.data.types));
	} catch (e) {
		dispatch(subscriptionActions.pending(false));
		dispatch(errorActions.receiveError(e));
	}
};

export const buySubscription = ({typeId, isItFree}: BuySubscriptionBody): AsyncAction => async (
	dispatch,
	_,
	{ protectedApi }
) => {
	try {
		dispatch(subscriptionActions.pending());

		const response = await protectedApi.buySubscription({ typeId, isItFree });

		if (response.data && response.data.expires) {
			dispatch(subscriptionActions.receiveSubscription(response.data.expires, response.data.wasFreeUsed));
		}
	} catch (e) {
		dispatch(subscriptionActions.pending(false));
		dispatch(errorActions.receiveError(e));
	}
};

export const getSubscriptionExpires = (): AsyncAction => async (
	dispatch,
	_,
	{ protectedApi }
) => {
	try {
		dispatch(subscriptionActions.pending());

		const response = await protectedApi.getSubscriptionExpires();

		if (response.data && response.data.expires) {
			dispatch(subscriptionActions.receiveSubscription(response.data.expires, response.data.wasFreeUsed));
		}
	} catch (e) {
		dispatch(subscriptionActions.pending(false));
		dispatch(errorActions.receiveError(e));
	}
};