import { ImmerReducer, createReducerFunction } from "immer-reducer";

import Film from "@/interfaces/Film";
import Link from "@/interfaces/NavigationLink";
import Mark from "@/interfaces/Mark";

interface FilmsState {
	films: Film[],
	pagesCount: number,
	genres: string[],
	links: Link[],
	marks: Mark[],
	pending: boolean
}

const initialState: FilmsState = {
	films: [],
	pagesCount: 0,
	genres: [],
	links: [],
	marks: [],
	pending: false
};

export class FilmsReducer extends ImmerReducer<FilmsState> {
	pending(status: boolean = true) {
		this.draftState.pending = status;
	}

	receiveGenres(genres: { genre: string }[]) {
		genres.forEach(({ genre }) => {
			this.draftState.genres.push(genre);
			this.draftState.links.push({
				path: `/films/${ genre.toLowerCase() }/1`,
				title: genre,
				exact: false
			});
			this.draftState.pending = false;
		})
	}

	receiveFilms(films: Film[], pagesCount: number) {
		this.draftState.films = films;
		this.draftState.pending = false;
		this.draftState.pagesCount = pagesCount;
	}

	receiveUserMarks(marks: Mark[]) {
		this.draftState.marks = marks;
	}

	updateFilmMark(filmId: number, newMark: number) {
		const updatedFilms = [];

		this.draftState.films.forEach(film => {
			if (film.id === filmId) film.mark = newMark;

			updatedFilms.push(film);
		})
	}

	// clearFilms() {
	// 	this.draftState.pending = true;
	// 	this.draftState.films = [];
	// }

	clearState() {
		this.draftState.films = [];
		this.draftState.genres = [];
		this.draftState.links = [];
		this.draftState.marks = [];
		this.draftState.pending = false;
	}
}

export default createReducerFunction(FilmsReducer, initialState);