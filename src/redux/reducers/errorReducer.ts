import { ImmerReducer, createReducerFunction } from "immer-reducer";

interface AuthState {
	errorMessage: string
}

const initialState: AuthState = {
	errorMessage: ''
};

export class ErrorReducer extends ImmerReducer<AuthState> {
	receiveError(errorMessage: string) {
		this.draftState.errorMessage = errorMessage;
	}

	removeError() {
		this.draftState.errorMessage = '';
	}
}

export default createReducerFunction(ErrorReducer, initialState);