import { ImmerReducer, createReducerFunction } from "immer-reducer";

import Payments from "@/interfaces/Payments";
import Type from "@/interfaces/SubscriptionType";

interface SubscriptionState {
	expires: string,
	pending: boolean,
	wasFreeUsed: number,
	payments: Payments[],
	types: Type[]
}

const initialState: SubscriptionState = {
	expires: '',
	pending: false,
	wasFreeUsed: 0,
	payments: [],
	types: []
};

export class SubscriptionReducer extends ImmerReducer<SubscriptionState> {
	pending(status: boolean = true) {
		this.draftState.pending = status;
	}

	receiveHistory(history: Payments[]) {
		this.draftState.payments = history;
		this.draftState.pending = false;
	}

	receiveTypes(types: Type[]) {
		this.draftState.types = types;
		this.draftState.pending = false;
	}

	receiveSubscription(expires: string, isItFree: number) {
		this.draftState.expires = expires;
		this.draftState.wasFreeUsed = isItFree;
		this.draftState.pending = false;
	}

	receiveWasFreeUsed(wasFreeUsed: number) {
		this.draftState.wasFreeUsed = wasFreeUsed;
	}

	clearState() {
		this.draftState.expires = '';
		this.draftState.pending = false;
		this.draftState.wasFreeUsed = 0;
		this.draftState.payments = [];
		this.draftState.types = [];
	}
}

export default createReducerFunction(SubscriptionReducer, initialState);