import { ImmerReducer, createReducerFunction } from "immer-reducer";

interface AuthState {
	token: string,
	previousToken: string,
	authPending: boolean
}

const initialState: AuthState = {
	token: '',
	previousToken: '',
	authPending: false
};

export class AuthReducer extends ImmerReducer<AuthState> {
	pending(status: boolean = true) {
		this.draftState.authPending = status;
	}

	authSuccessful(token: string, previousToken?: string) {
		this.draftState.token = token;
		this.draftState.authPending = false;

		if (previousToken) this.draftState.previousToken = previousToken;
	}

	clearState() {
		this.draftState.token = '';
		this.draftState.previousToken = '';
		this.draftState.authPending = false;
	}
}

export default createReducerFunction(AuthReducer, initialState);