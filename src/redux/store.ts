import {createStore, applyMiddleware, compose, combineReducers} from "redux";
import thunk from "redux-thunk";
import {createBrowserHistory} from 'history';
import {connectRouter, routerMiddleware} from 'connected-react-router';
import auth from '@/redux/reducers/authReducer';
import subscription from '@/redux/reducers/subscriptionReducer';
import error from '@/redux/reducers/errorReducer';
import films from '@/redux/reducers/filmsReducer';
import MainApi from "@/api/MainApi";

import { AuthActions } from "@/redux/actions/authActions";
import { ErrorActions } from "@/redux/actions/errorActions";
import { SubscriptionActions } from "@/redux/actions/subscriptionActions";
import { FilmActions } from "@/redux/actions/filmActions";
import MainProtectedApi from "@/api/MainProtectedApi";

export const api = {
	mainApi: MainApi.getInstance(),
	protectedApi: MainProtectedApi.getInstance()
};

export const history = createBrowserHistory();

const rootReducer = combineReducers({
	auth,
	subscription,
	error,
	films,
	router: connectRouter(history)
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancer = composeEnhancers(applyMiddleware(
	routerMiddleware(history),
	thunk.withExtraArgument(api)
));

export type State = ReturnType<typeof rootReducer>;
export type Actions = AuthActions | SubscriptionActions | ErrorActions | FilmActions;

export default createStore(rootReducer, enhancer);