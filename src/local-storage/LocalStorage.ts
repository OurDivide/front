interface IStorage {
	getItem(key: string): string | null;
	setItem(key: string, value: string): void;
	removeItem(key: string): void;
}
abstract class LocalStorage<T extends string> {
	private readonly storage: IStorage | null;

	protected constructor(getStorage = (): IStorage | null => (typeof window === 'undefined' ? null : window.localStorage)) {
		this.storage = getStorage();
	}

	protected get(key: T): any {
		return this.storage?.getItem(key);
	}

	protected set(key: T, value: string): void {
		this.storage?.setItem(key, value);
	}

	protected clearItem(key: T): void {
		this.storage?.removeItem(key);
	}

	protected clearItems(keys: T[]): void {
		keys.forEach((key) => this.clearItem(key));
	}
}
export default LocalStorage;