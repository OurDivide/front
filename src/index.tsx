import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from "connected-react-router";
import { createGlobalStyle } from "styled-components";
import { normalize } from "styled-normalize";

import store, { history } from '@/redux/store';
import App from '@/App';
import Lato400 from '@/assets/fonts/Lato-Regular.ttf';
import Lato300 from '@/assets/fonts/Lato-Light.ttf';
import Lato700 from '@/assets/fonts/Lato-Bold.ttf';

const GlobalStyle = createGlobalStyle`
  ${ normalize };
  
  @font-face {
    font-family: "Lato";
    src: url(${ Lato400 }) format("truetype");
    font-weight: 400;
	}
	
	@font-face {
    font-family: "Lato";
    src:url(${ Lato700 }) format("truetype");
    font-weight: 700;
	}
	
	@font-face {
	  font-family: "Lato";
	  src: url(${ Lato300 }) format("truetype");
	  font-weight: 300;
	}

  * {
    font-family: "Lato", sans-serif;
    color: #333;
  }

  body {
    margin: 0;
    height: 100vh;
    overflow-x: hidden;
    background-color: #efefef;
  }
  
  #root {
    height: 100%;
  }
  
  a {
  	text-decoration: none;
  }
  
  ul {
  	margin: 0;
  	padding: 0;
  	list-style: none;
  }
  
  img {
  	width: 100%;
  	height: auto;
  }
`;

ReactDOM.render(
	<React.StrictMode>
		<Provider store={ store }>
			<ConnectedRouter history={ history }>
				<GlobalStyle/>
				<App/>
			</ConnectedRouter>
		</Provider>
	</React.StrictMode>,
	document.getElementById('root')
);
