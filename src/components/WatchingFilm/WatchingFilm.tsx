import React, { useEffect } from 'react';
import { useParams, useLocation } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { Link, NavLink } from "react-router-dom";

import SectionTitle from "@/components/UI/SectionTitle/SectionTitle";
import StarList from "@/components/UI/StarList/StarList";
import { getSubscriptionExpires } from "@/redux/actions/subscriptionActions";
import { selectExpires } from "@/redux/selectors/subscriptionSelector";
import { selectMarks, selectFilms } from "@/redux/selectors/filmsSelector";

interface UrlParams {
	id: string
}

interface LocationState {
	title: string
	usersMark: string,
	releaseYear: number,
	genres: string,
	views: number,
	description: string
}

const WatchingFilm: React.FC = () => {
	const { id } = useParams<UrlParams>();
	const { state: { title, releaseYear, genres, description } } = useLocation<LocationState>();
	const dispatch = useDispatch();
	const expires = useSelector(selectExpires);
	const marks = useSelector(selectMarks);
	const films = useSelector(selectFilms);

	useEffect(() => {
		if (!expires) dispatch(getSubscriptionExpires());
	}, [dispatch, expires]);

	let currentMark,
		currentUserMark: number | string = 'No Mark';

	marks.forEach(markObj => {
		if (markObj.mediaId === +id) currentMark = markObj.mark
	});

	if (!currentMark) currentMark = <StarList filmId={ id }/>;

	films.forEach(film => {
		if (film.id === +id && film.mark) currentUserMark = film.mark.toFixed(1);
	});

	return (
		<WatchingContainer>
			<SectionTitle isWithBackArrow>{ title }</SectionTitle>
			<InfoContainer>
				<div>
					<InfoTitle>Information:</InfoTitle>
					<InfoItem>Genres: { genres }</InfoItem>
					<InfoItem>Release Year: { releaseYear }</InfoItem>
					<InfoItem>Users Mark: { currentUserMark }</InfoItem>
					{ expires && expires !== 'false' ? <div>Your Mark: { currentMark }</div> : null }
				</div>
				<div>
					<InfoTitle>Description:</InfoTitle>
					{ description }
				</div>
			</InfoContainer>
			{ expires && expires !== 'false' ? (
				<VideoPlayer controls controlsList='nodownload'>
					<source src={ `http://localhost:3001/api/watch/${ id }` } type="video/mp4"/>
				</VideoPlayer>
			) : <p>You haven't subscribe yet. But you can <StyledLink to='/profile/subscription'>Buy it now.</StyledLink></p> }
		</WatchingContainer>
	);
};

const WatchingContainer = styled.div`
	margin: 0 auto;
	padding: 40px 0 140px 0;
	max-width: 1024px;
	width: 90%;
`;

const VideoPlayer = styled.video`
	display: block;
	margin: 0 auto;
	width: 100%;
	height: auto;
`;

const InfoContainer = styled.div`
	margin-bottom: 40px;
	
	div:first-child {
		margin-bottom: 30px;
		padding-bottom: 20px;
		border-bottom: 1px solid #c0c0c0;
	}
	
	@media (min-width: 768px) {
		display: flex;
		justify-content: space-between;
		
		div:first-child {
			margin-bottom: 0;
			padding-bottom: 0;
			width: 30%;
			border-bottom: none;
		}
		
		div:last-child {
			width: 60%;
		}
	}
`;

const InfoTitle = styled.h3`
	font-size: 18px;
	font-weight: 700;
`;

const InfoItem = styled.p`
	margin: 0 0 10px 0;
`;

const StyledLink = styled(Link)`
	color: #789cfb;
	transition: .2s;
	
	&:hover {
		color: #fb5b53;
	}
`;

export default WatchingFilm;