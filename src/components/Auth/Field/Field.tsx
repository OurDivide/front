import React from 'react';
import styled from "styled-components";

import {  changeHandler } from "@/components/Auth/Auth";
import Input from "@/components/UI/Input/Input";
import FieldLengthError from "@/components/Auth/FieldLengthError/FieldLengthError";
import Field from "@/interfaces/Field";

interface Props {
	fieldConfig: Field,
	changeHandler: changeHandler
}

const Field: React.FC<Props> = ({ fieldConfig, changeHandler }) => {
	return (
		<InputWrapper>
			<Input fieldConfig={ fieldConfig }
			       changeHandler={ changeHandler }/>
			{ fieldConfig.lengthError ? <FieldLengthError minLength={ fieldConfig.minLength }/> : null }
		</InputWrapper>
	);
};

const InputWrapper = styled.div`
	margin: 0 auto;
  width: 80%;
  max-width: 210px;
  min-width: 160px;
  position: relative;
`;

export default Field;