import React from 'react';

import styled from "styled-components";

interface Props {
	minLength: number
}

const FieldLengthError = ({ minLength } : Props) => (
	<ErrorText>At least { minLength } symbols</ErrorText>
);

const ErrorText = styled.p`
	position: absolute;
  bottom: -6px;
  left: 0;
  font-size: 11px;
  color: #fb5b53;
`;

export default FieldLengthError;