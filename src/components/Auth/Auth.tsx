import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import styled from "styled-components";

import { authenticate } from "@/redux/actions/authActions";
import Field from "@/components/Auth/Field/Field";
import useInputWithConfig from "@/hooks/useInputWithConfig";

export type changeHandler = (
	evt: React.ChangeEvent<HTMLInputElement>,
) => void

const Auth: React.FC = () => {
	const dispatch = useDispatch();
	const [isSignIn, setIsSignIn] = useState<boolean>(true);
	const [login, setLogin] = useInputWithConfig({
			type: 'text',
			minLength: 6,
			placeholder: 'Login'
		}),
		[email, setEmail] = useInputWithConfig({
			type: 'email',
			minLength: 6,
			placeholder: 'Email'
		}),
		[password, setPassword] = useInputWithConfig({
			type: 'password',
			minLength: 8,
			placeholder: 'Password'
		});

	const areFieldInterfaceFilled = () => {
		const isEmailFilled = isSignIn ? true : email.value && !email.lengthError;

		return login.value && !login.lengthError &&
			password.value && !password.lengthError &&
			isEmailFilled
	};

	const signInHandler = (event: React.SyntheticEvent) => {
		event.preventDefault();

		if (areFieldInterfaceFilled()) {
			dispatch(authenticate({
				login: login.value,
				password: password.value
			}));
		}
	};

	const signUpHandler = (event: React.SyntheticEvent) => {
		event.preventDefault();

		if (areFieldInterfaceFilled()) {
			dispatch(authenticate({
				login: login.value,
				password: password.value,
				email: email.value
			}));
		}
	};

	return (
		<AuthContainer>
			<AuthForm>
				<h2>{ isSignIn ? 'Sign In' : 'Sign Up' }</h2>
				{
					isSignIn
						? <AuthInfo>
							Not a member yet? Please <ChangeMode onClick={ () => setIsSignIn(false) }>Sign Up</ChangeMode>
						</AuthInfo>
						: <AuthInfo>
							Already registered? You could <ChangeMode onClick={ () => setIsSignIn(true) }>Sign In</ChangeMode>
						</AuthInfo>
				}
				<Field fieldConfig={ login }
				       changeHandler={ setLogin }/>
				{
					!isSignIn
						? <Field fieldConfig={ email }
						         changeHandler={ setEmail }/>
						: null
				}
				<Field fieldConfig={ password }
				       changeHandler={ setPassword }/>
				<SubmitButton type="submit"
				        disabled={ !areFieldInterfaceFilled() }
				        onClick={ isSignIn ? signInHandler : signUpHandler }>
					{ isSignIn ? 'Sign In' : 'Sign Up' }
				</SubmitButton>
			</AuthForm>
		</AuthContainer>
	);
};

const AuthContainer = styled.div`
	display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: calc(100vh - 80px);
  background: linear-gradient(to bottom left, rgba(255, 0, 97, 0.6), rgba(255, 35, 26, 0.6), rgba(255, 159, 32, 0.6));
`;

const AuthForm = styled.form`
	display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px 20px 30px 20px;
  max-width: 360px;
  width: 30%;
  min-width: 220px;
  text-align: center;
  border-radius: 25px;
  background-color: #fff;
  box-shadow: rgba(0, 0, 0, 0.24) 0 3px 8px;
  
  h2 {
  	font-weight: 500;
  }
`;

const AuthInfo = styled.p`
	margin-bottom: 30px;
  font-weight: 300;
`;

const ChangeMode = styled.span`
		color: #6a6cfb;
		transition: .2s;

		&:hover {
			cursor: pointer;
			color: #6fa4fb;
		}
`;

const SubmitButton = styled.button`
	cursor: pointer;
  margin-top: 20px;
  font-size: 15px;
  border: none;
  background-color: transparent;
`;

export default Auth;