import React, { useEffect } from 'react';
import { useDispatch } from "react-redux";

import { logout } from "@/redux/actions/authActions";

const Logout: React.FC = () => {
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(logout());
	});

	return null;
};

export default Logout;