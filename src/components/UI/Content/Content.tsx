import React from 'react';
import styled from "styled-components";

const Content: React.FC = ({ children }) => {
	return (
		<Container>
			{ children }
		</Container>
	);
};

const Container = styled.div`
	width: 90%;
	max-width: 710px;
	
	@media (min-width: 768px) {
		margin-left: 80px;
	}
`;

export default Content;