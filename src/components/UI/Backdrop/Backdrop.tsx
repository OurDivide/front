import React from 'react';
import styled from "styled-components";

interface Props {
	onClickHandler: () => void,
	shouldBeShowed?: boolean
}

const Backdrop: React.FC<Props> = ({ onClickHandler }) => {
	return <BackdropLayout onClick={ onClickHandler }/>;
};

const BackdropLayout = styled.div`
	position: absolute;
	width: 100vw;
	height: 100vh;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	background-color: rgba(0, 0, 0, .4);
	z-index: 300;
`;

export default Backdrop;