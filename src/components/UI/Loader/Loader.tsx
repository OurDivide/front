import React from 'react';
import styled, { keyframes } from 'styled-components';

const Loader: React.FC = () => <DualRingSpinner/>;

const motion =  keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

const DualRingSpinner = styled.div`
	position: absolute;
	top: calc(50% - 32px);
	right: calc(50% - 32px);
  display: inline-block;
  width: 64px;
  height: 64px;
  
  :after {
    content: ' ';
    display: block;
    width: 46px;
    height: 46px;
    margin: 1px;
    border: 5px solid #00bfff;
    border-radius: 50%;
    border-color: #00bfff transparent #00bfff transparent;
    animation: ${ motion } 1.2s linear infinite;
  }
`;

export default Loader;