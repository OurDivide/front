import React from 'react';
import styled from "styled-components";

import Link from "@/interfaces/NavigationLink";
import SubNavigationItem from "@/components/UI/SubNavigation/SubNavigationItem/SubNavigationItem";

interface Props {
	links: Link[]
}

const SubNavigation: React.FC<Props> = ({ links }) => {
	return (
		<List>
			{
				links.map((link, i) => (
					<SubNavigationItem linkInfo={ link } key={ i }/>
				))
			}
		</List>
	);
};

const List = styled.ul`
	display: none;
	
	@media (min-width: 768px) {
		display: flex;
		flex-direction: column;
		padding-right: 10px;
		width: 220px;
		border-right: 1px solid #c0c0c0;
	}
`;

export default SubNavigation;