import React from 'react';
import { NavLink } from "react-router-dom";
import styled from "styled-components";

import Link from "@/interfaces/NavigationLink";

interface Props {
	linkInfo: Link
}

const SubNavigationItem: React.FC<Props> = ({ linkInfo }) => {
	const logout = linkInfo.title === 'Logout' ? { logout: true } : { };

	return (
		<ListItem>
			<StyledNavLink to={ linkInfo.path }
			               exact={ linkInfo.exact }
						   { ...logout }>
				{ linkInfo.title }
			</StyledNavLink>
		</ListItem>
	);
};

const ListItem = styled.li`
	padding: 6px;
`;

const StyledNavLink = styled(NavLink).attrs({ activeClassName: 'active' })<{ logout?: boolean }>`
	position: relative;
	font-size: 18px;
	color: ${ ({ logout }) => logout ? '#fb5b53' : '#171717' };
	overflow-wrap: break-spaces;
	
	&::after {
		content: '';
		position: absolute;
		bottom: -2px;
		left: 0;
		width: 0;
		height: 1px;
		border-radius: 5px;
		background-color: #3cccfb;
		transition: width 0.2s ease-out;
		visibility: hidden;
	}
	
	&:hover::after {
		width: 100%;
		visibility: visible;
	}
	
	&.active::before {
		content: '';
		position: absolute;
		top: calc(50% - 2px);
		left: -14px;
		width: 5px;
		height: 5px;
		border-radius: 50%;
		background-color: #3cccfb;
	}
`;

export default SubNavigationItem;