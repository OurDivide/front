import React from 'react';
import styled from "styled-components";
import { useHistory } from "react-router";

interface Props {
	isWithBackArrow?: boolean
}

const SectionTitle: React.FC<Props> = ({ children, isWithBackArrow = false }) => {
	const history = useHistory();

	return (
		<Title>
			{ isWithBackArrow ? (
				<BackArrow onClick={ () => history.goBack() }>
					⇐
				</BackArrow>
			) : null }
			{ children }
		</Title>
	);
};

const Title = styled.h2`
	padding-bottom: 25px;
	font-size: 26px;
	text-transform: capitalize;
	border-bottom: 1px solid #c0c0c0;
`;

const BackArrow = styled.span`
	padding-right: 15px;
	font-size: 30px;
	transition: .1s ease-in-out;
	cursor: pointer;
	
	&:hover {
		color: #fb5b53;
	}
`;

export default SectionTitle;