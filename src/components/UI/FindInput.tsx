import React, { useState, useEffect } from 'react';
import { useDispatch } from "react-redux";
import styled from "styled-components";
import { push } from "connected-react-router";

import useInput from "@/hooks/useInput";
import Timeout = NodeJS.Timeout;

interface Props {
	basePath: string
}

const FindInput: React.FC<Props> = ({ basePath }) => {
	const [isFirstRender, setIsFirstRender] = useState(true);
	const [value, changeHandler] = useInput();
	const dispatch = useDispatch();

	const findHandler = () => dispatch(push(basePath + value + '/1'));

	let newTimer: Timeout;

	useEffect(() => {
		if (!isFirstRender) {
			if (value) {
				const newTimer = setTimeout(() => {
					findHandler();
				}, 2000);

				return () => clearTimeout(newTimer);
			} else dispatch(push(basePath));
		} else setIsFirstRender(false);
	}, [dispatch, history, basePath, value]);

	const onKeyPressHandler = (evt: React.KeyboardEvent<HTMLInputElement>) => {
		if (evt.key === 'Enter' && value) {
			Promise.all([clearTimeout(newTimer), findHandler()]);
		}
	};

	return <Input type="text"
	              value={ value }
	              onKeyPress={ onKeyPressHandler }
	              placeholder='Find the film'
	              onChange={ changeHandler }/>;
};

const Input = styled.input`
	box-sizing: border-box;
	display: block;
	margin-bottom: 30px;
	padding: 6px 10px;
	width: 100%;
	font-size: 17px;
	border: none;
	border-bottom: 3px solid #61dafb;
	transition: .2s;

	&:focus {
		outline: none;
		border-bottom: 3px solid #789cfb;
	}
`;

export default FindInput;