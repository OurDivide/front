import React, { useState } from 'react';
import { useDispatch } from "react-redux";
import { push } from "connected-react-router";
import styled from "styled-components";

import { range } from "@/utils/utils";

type NeighborsCount = 1 | 2;
type Block = 'LEFT' | 'RIGHT' | number;

interface Props {
	basePath: string,
	pagesCount: number,
	currentPage: number
}

const Pagination: React.FC<Props> = ({ basePath, pagesCount, currentPage }) => {
	const dispatch = useDispatch();

	const neighborsCount: NeighborsCount = 1;
	const totalVisiblePages = (neighborsCount * 2) + 3;
	const totalVisibleBlocks = totalVisiblePages + 2;

	let blocks: Block[] = [];

	if (pagesCount > totalVisibleBlocks) {
		const startPage = Math.max(2, currentPage - neighborsCount);
		const endPage = Math.min(pagesCount - 1, currentPage + neighborsCount);
		const pages = range(startPage, endPage);

		const hasLeftControl = startPage > 3;
		const hasRightControl = (pagesCount - endPage) > 2;

		if (hasLeftControl && !hasRightControl) {
			blocks = [1, 'LEFT', ...range(pagesCount - (2 + (neighborsCount * 2)), pagesCount - 1), pagesCount]
		}

		if (!hasLeftControl && hasRightControl) {
			blocks = [1, ...range(2, 2 + (1 + (neighborsCount * 2))), 'RIGHT', pagesCount];
		}

		if (hasLeftControl && hasRightControl) {
			blocks = [1, 'LEFT', ...pages, 'RIGHT', pagesCount];
		}
	} else blocks = range(1, pagesCount);

	const onClickHandler = (paginationItem: Block) => {
		let page = currentPage;

		switch (paginationItem) {
			case 'LEFT': {
				page -= ((neighborsCount * 2) + 1);
				break;
			}
			case 'RIGHT': {
				page += ((neighborsCount * 2) + 1);
				break;
			}
			default: {
				page = paginationItem;
			}
		}

		page = Math.max(0, Math.min(pagesCount, page));

		dispatch(push(basePath + page));
	};

	return (
		<PaginationList>
			{
				blocks.map((block, index) => {
					const isActive = typeof block === 'number' && currentPage === block;

					if (block === 'LEFT') return (
						<PaginationItem key={ index }
						                isActive={ isActive }
						                onClick={ () => onClickHandler(block) }>
							&#171;
						</PaginationItem>
					);

					if (block === 'RIGHT') return (
						<PaginationItem key={ index }
						                isActive={ isActive }
						                onClick={ () => onClickHandler(block) }>
							&#187;
						</PaginationItem>
					);

					return (
						<PaginationItem key={ index }
						                isActive={ isActive }
						                onClick={ () => onClickHandler(block) }>
							{ block }
						</PaginationItem>
					);
				})
			}
		</PaginationList>
	);
};

const PaginationList = styled.ul`
	display: flex;
	margin: 0 auto;
	width: max-content;
	background-color: #fff;
	
	li:first-child {
		border-left: 1px solid #797979;
		border-top-left-radius: 5px;
		border-bottom-left-radius: 5px;
	}
	
	li:last-child {
		border-top-right-radius: 5px;
		border-bottom-right-radius: 5px;
	}
`;

const PaginationItem = styled.li<{ isActive: boolean }>`
	padding: 6px 12px;
	width: 20px;
	color: ${ ({ isActive }) => isActive ? '#fb5b53' : 'inherit' };
	text-align: center;
	border-top: 1px solid #797979; 
	border-right: 1px solid #797979;
	border-bottom: 1px solid #797979;
	cursor: pointer;
	transition: .2s ease-in-out;
	
	&:hover {
		background-color: #abfbf1;
	}
`;

export default Pagination;