import React from 'react';
import { useDispatch } from "react-redux";

import Backdrop from "@/components/UI/Backdrop/Backdrop";
import { errorActions } from "@/redux/actions/errorActions";
import styled from "styled-components";

interface Props {
	message: string
}

const ModalError: React.FC<Props> = ({ message }) => {
	const dispatch = useDispatch();

	const onClickHandler = () => {
		dispatch(errorActions.removeError());
	};

	return (
		<React.Fragment>
			<ModalWindow>
				<Title>Error</Title>
				<Message>{ message }</Message>
			</ModalWindow>
			<Backdrop onClickHandler={ onClickHandler }/>
		</React.Fragment>
	);
};

const ModalWindow = styled.div`
	box-sizing: border-box;
	position: absolute;
	top: 50%;
	left: 50%;
	padding: 30px 20px;
	width: 30%;
	min-width: 280px;
	max-width: 500px;
	border-radius: 20px;
	background-color: #fff;
	transform: translate(-50%, -50%);
	z-index: 500;
`;

const Title = styled.h3`
	padding-bottom: 20px;
	font-size: 26px;
	color: #fb5b53;
	border-bottom: 1px solid #c0c0c0;
`;

const Message = styled.p`
	color: #838383;
	font-weight: 300;
`;

export default ModalError;