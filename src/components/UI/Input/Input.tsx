import React from 'react';
import styled from "styled-components";

import Field from "@/interfaces/Field";
import { changeHandler } from "@/components/Auth/Auth";

interface Props {
	fieldConfig: Field,
	changeHandler: changeHandler
}

interface StyledProps extends React.HTMLProps<HTMLInputElement> {
	lengthError: boolean
}

const Input: React.FC<Props> = ({ fieldConfig, changeHandler }) => {
	return (
		<InputField type={ fieldConfig.type }
		            value={ fieldConfig.value }
		            placeholder={ fieldConfig.placeholder }
		            lengthError={ fieldConfig.lengthError }
		            onChange={ changeHandler }/>
	);
};

const InputField = styled.input<StyledProps>`
		box-sizing: border-box;
		display: block;
		margin-bottom: 20px;
		padding: 3px 10px;
		width: 100%;
		font-size: 17px;
		border: none;
		border-bottom: 3px solid ${ ({ lengthError }) => lengthError ? '#fb5b53' : '#61dafb' };
		transition: .2s;
		
		:focus {
			outline: none;
			border-bottom: 3px solid #789cfb;
		}
`;

export default Input;