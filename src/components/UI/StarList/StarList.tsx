import React, { useState } from 'react';
import styled from "styled-components";
import { useDispatch } from "react-redux";

import { rateFilm } from "@/redux/actions/filmActions";

interface Props {
	filmId: string
}

const StarList: React.FC<Props> = ({ filmId }) => {
	const [hovered, setHovered] = useState<number | null>(null);
	const dispatch = useDispatch();

	let arr = [];

	for (let i = 0; i < 5; i++) {
		let starColor = '#333';

		if (typeof hovered === 'number' && i <= hovered) starColor = '#ffbd05';

		arr.push(
			<Star key={ i }
			      onMouseOver={ () => setHovered(i) }
						onMouseOut={ () => setHovered(null) }
						style={{ color: starColor }}
						onClick={() => dispatch(rateFilm({
							filmId: +filmId,
							mark: i + 1
						}))}>
				★
			</Star>
		)
	}

	return (
		<List>
			{ arr }
		</List>
	);
};

const List = styled.ul`
	display: inline-block;
	gap: 5px;
`;

const Star = styled.li`
	display: inline;
	font-size: 26px;
	font-weight: 700;
	cursor: pointer;
	transition: .2s;
`;

export default StarList;