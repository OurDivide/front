import React, { useEffect } from 'react';
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";

import { selectPayments, selectPending } from "@/redux/selectors/subscriptionSelector";
import { getPaymentsHistory } from "@/redux/actions/subscriptionActions";
import reloadImage from '@/assets/images/reload.png';

const Payments: React.FC = () => {
	const dispatch = useDispatch();
	const payments = useSelector(selectPayments);
	const pending = useSelector(selectPending);

	let paymentsRow: JSX.Element[] = [],
		paymentsTable;

	useEffect(() => {
		if (!payments || (payments && !payments.length)) dispatch(getPaymentsHistory());
	}, [dispatch, payments]);

	if (payments) {
		paymentsRow = payments.slice(0).reverse().map(({ duration, price, date }, i) => (
			<PaymentElement key={ i }>
				<td>
					{ new Date(+date).toDateString() }
				</td>
				<td>
					{ duration } { duration === 1 ? 'Day' : 'Days' }
				</td>
				<td>
					{ price === 0 ? 'Free' : `${ price }$` }
				</td>
			</PaymentElement>
		));

		paymentsTable = (
			<Table>
				<thead>
				<TitleRow>
					<td>Date</td>
					<td>Duration</td>
					<td>Price</td>
				</TitleRow>
				</thead>
				<tbody>
				{ paymentsRow }
				</tbody>
			</Table>
		)
	}

	return (
		<React.Fragment>
			<TitleWrapper>
				<Title>Payments History</Title>
				<ReloadButton pending={ pending }
				              onClick={ () => dispatch(getPaymentsHistory()) }>
					Reload history
				</ReloadButton>
			</TitleWrapper>
			{ payments ? paymentsTable : <Message>You haven't any payments</Message> }
		</React.Fragment>
	);
};

const Table = styled.table`
	width: 100%;
	
	td:not(:last-child) {
		padding: 15px 20px;
		width: 40%;
	}
	
	@media (min-width: 768px) {
		td {
			padding: 20px 30px;
		}
	}
`;

const TitleRow = styled.tr`
	td {
		color: #686868;
	}
`;

const PaymentElement = styled.tr`
	box-sizing: border-box;
	width: 100%;
	max-width: 960px;
	border-radius: 5px;
	box-shadow: rgba(50, 50, 93, 0.25) 0 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px;
`;

const TitleWrapper = styled.div`
	display: flex;
	gap: 15px;
	align-items: center;
	padding-bottom: 25px;
	border-bottom: 1px solid #c0c0c0;
`;

const Title = styled.h2`
	margin-bottom: 0;
	font-size: 26px;
	
	& + div {
		margin-top: auto;
	}
`;

const ReloadButton = styled.div<{ pending: boolean }>`
	width: 24px;
	height: 24px;
	font-size: 0;
	background-image: url(${ reloadImage });
	background-color: transparent;
	transition: all .5s ease-in-out;
	transform: ${ ({ pending }) => pending ? 'rotate(360deg)' : 'none' };
	cursor: pointer;
`;

const Message = styled.p`
	color: #686868;
`;

export default Payments;