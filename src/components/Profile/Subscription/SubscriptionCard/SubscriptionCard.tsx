import React from 'react';
import styled from "styled-components";
import { useDispatch } from "react-redux";

import SubscriptionType from "@/interfaces/SubscriptionType";
import { buySubscription } from "@/redux/actions/subscriptionActions";

const SubscriptionCard: React.FC<SubscriptionType> = ({ id, duration, price }) => {
	const dispatch = useDispatch();
	const monthCount = Math.floor(duration / 30),
		isItFree = +(price === 0);

	let word;

	if (monthCount) word = monthCount === 1 ? 'Month' : 'Months';
	else word = duration === 1 ? 'Day' : 'Days';

	if (isItFree) word += ' For Free';

	const buttonClickHandler = () => dispatch(buySubscription({ typeId: id, isItFree }));

	return (
		<Card isItFree={ isItFree }>
			<Duration>{ monthCount ? monthCount : duration }</Duration>
			<DurationWord>{ word }</DurationWord>
			<CardFooter>
				<span>{ isItFree ? 'Free' : `${ price }$` }</span>
				<BuyButton onClick={ buttonClickHandler }>Buy Now</BuyButton>
			</CardFooter>
		</Card>
	);
};

const Card = styled.div<{ isItFree: number }>`
	box-sizing: border-box;
	margin: 0 auto;
	padding: 15px;
	width: 100%;
	max-width: 300px;
	font-size: 18px;
	border-radius: 15px;
	background-color: ${ ({ isItFree }) => isItFree ? '#333' : '#fff' };
	box-shadow: rgba(50, 50, 93, 0.25) 0 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px;
	
	div *, span {
		color: ${ ({ isItFree }) => isItFree ? '#fff' : '#333' };
	}
	
	@media (min-width: 768px) {
		width: 50%;
	}
`;

const Duration = styled.span`
	display: block;
	font-size: 40px;
	font-weight: 700;
	text-align: center;
`;

const DurationWord = styled.span`
	display: block;
	margin-bottom: 30px;
	font-size: 30px;
	font-weight: 700;
	text-align: center;
`;

const CardFooter = styled.div`
	display: flex;
	justify-content: space-between;
`;

const BuyButton = styled.button`
	color: #fb5b53;
	border: none;
	background-color: transparent;
	cursor: pointer;
	transition: 0.2s;
	
	&:hover {
		text-shadow: 1px 1px 1px #fb5b53;
	}
`;

export default SubscriptionCard;