import React, { useEffect } from 'react';
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";

import { getSubscriptionTypes, getSubscriptionExpires, subscriptionActions } from "@/redux/actions/subscriptionActions";
import { selectExpires, selectTypes, selectWasFreeUsed } from "@/redux/selectors/subscriptionSelector";
import SubscriptionCard from "@/components/Profile/Subscription/SubscriptionCard/SubscriptionCard";
import SectionTitle from "@/components/UI/SectionTitle/SectionTitle";

const Subscription = () => {
	const dispatch = useDispatch();
	const expiresFromSelector = useSelector(selectExpires);
	const types = useSelector(selectTypes);
	const wasFreeUsed = useSelector(selectWasFreeUsed);
	const expires = expiresFromSelector === 'false' || expiresFromSelector === '' ? null : new Date(+expiresFromSelector);

	let currentDate = new Date(),
		subscriptionLeft;

	if (expires) {
		subscriptionLeft = Math.ceil(Math.abs(expires.getTime() - currentDate.getTime()) / (1000 * 3600 * 24));

		if (subscriptionLeft <= 3) subscriptionLeft = subscriptionLeft + (subscriptionLeft <= 1 ? ' day' : ' days');
		else subscriptionLeft = null;
	}

	useEffect(() => {
		if (!expiresFromSelector) dispatch(getSubscriptionExpires());
		if (types && !types.length) dispatch(getSubscriptionTypes());
	}, [dispatch, types, expiresFromSelector]);

	return (
		<SubscriptionContainer>
			<SectionTitle>Your Subscription Status</SectionTitle>
			<SubscriptionInfo>
				{ expires ? `You're subscriber to: ${ expires.toDateString() }` : `You haven't subscriber yet` }
			</SubscriptionInfo>
			{ subscriptionLeft ? (
				<SubscriptionLeftInfo>
					{ expires ? `There are ${ subscriptionLeft } left. Please extend your subscription.` : null }
				</SubscriptionLeftInfo>
			) : null }
			<SectionTitle>Buy or Extend Your Subscription</SectionTitle>
			<Cards>
				{
					types.map((type, i) => {
						if (wasFreeUsed && type.price === 0) return null;

						return (
							<SubscriptionCard key={ i }
							                  id={ type.id }
							                  duration={ type.duration }
							                  price={ type.price }/>
						)
					})
				}
			</Cards>
		</SubscriptionContainer>
	);
};

const SubscriptionContainer = styled.div`
	width: 100%;
	max-width: 710px;
`;

const SubscriptionInfo = styled.p`
	margin-bottom: 40px;
	color: #686868;
`;

const SubscriptionLeftInfo = styled.p`
	margin-bottom: 40px;
	color: #fb5b53;
`;

const Cards = styled.div`
	display: flex;
	flex-wrap: wrap;
	gap: 25px;
`;

export default Subscription;