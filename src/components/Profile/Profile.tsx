import React from 'react';
import styled from "styled-components";
import { Redirect, Switch, Route } from "react-router";

import Link from "@/interfaces/NavigationLink";
import SubNavigation from "@/components/UI/SubNavigation/SubNavigation";
import Subscription from "@/components/Profile/Subscription/Subscription";
import Payments from "@/components/Profile/Payments/Payments";
import Content from "@/components/UI/Content/Content";

export const profileLinks: Link[] = [
	{
		path: '/profile/subscription',
		title: 'Subscription',
		exact: false
	},
	{
		path: '/profile/payments',
		title: 'Payments',
		exact: false
	},
	{
		path: '/logout',
		title: 'Logout',
		exact: false
	}
];

const Profile: React.FC = () => {
	return (
		<ProfileContainer>
			<Route path='/profile' exact render={ () => <Redirect to='/profile/subscription'/> }/>
			<SubNavigation links={ profileLinks }/>
			<Content>
				<Switch>
					<Route path='/profile/subscription' render={ () => <Subscription/> }/>
					<Route path='/profile/payments' render={ () => <Payments/> }/>
				</Switch>
			</Content>
		</ProfileContainer>
	);
};

const ProfileContainer = styled.div`
	display: flex;
	margin: 0 auto;
	padding: 40px 0;
	max-width: 1024px;
	width: 90%;
`;

export default Profile;