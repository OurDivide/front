import React, { useEffect } from 'react';
import styled from "styled-components";
import { Route, Switch, Redirect } from "react-router";
import { useSelector, useDispatch } from "react-redux";

import SubNavigation from "@/components/UI/SubNavigation/SubNavigation";
import FilmsList from "@/components/Films/FilmsList/FilmsList";
import Content from "@/components/UI/Content/Content";
import { selectLinks } from "@/redux/selectors/filmsSelector";
import { selectToken } from "@/redux/selectors/authSelector";
import { getMarks } from "@/redux/actions/filmActions";
import FindInput from "@/components/UI/FindInput";

const Films: React.FC = () => {
	const filmsLink = useSelector(selectLinks);
	const token = useSelector(selectToken);
	const dispatch = useDispatch();

	useEffect(() => {
		if (token) dispatch(getMarks())
	}, [dispatch, token]);

	return (
		<FilmsPage>
			<Route exact path='/films' render={() => <Redirect to={ `/films/anime/1` }/> }/>
			<NavSection>
				<FindInput basePath='/films/' />
				<SubNavigation links={ filmsLink }/>
			</NavSection>
			<Content>
				<Switch>
					<Route path='/films/:genreParam/:pageParam' render={ () => <FilmsList/> }/>
				</Switch>
			</Content>
		</FilmsPage>
	);
};

const FilmsPage = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	margin: 0 auto;
	padding: 40px 0;
	max-width: 1024px;
	width: 90%;
	
	@media(min-width: 768px) {
		flex-direction: row;
		align-items: flex-start;
	}
`;

const NavSection = styled.section`
	padding-top: 30px;
`;

export default Films;