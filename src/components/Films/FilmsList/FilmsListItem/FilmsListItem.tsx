import React from 'react';
import styled from "styled-components";
import { Link } from "react-router-dom";

interface Props {
	id: number,
	title: string,
	mark: number,
	releaseYear: number,
	genres: string,
	previewPath: string,
	description: string
}

const FilmsListItem: React.FC<Props> = ({
	id, title, mark, releaseYear,
	genres, previewPath, description
}) => {
	let usersMark = 'No Mark';

	if (mark) {
		usersMark = mark.toFixed(1)
	}

	return (
		<FilmContainer>
			<Link to={{
				pathname: `/watch/${ id }`,
				state: { title, releaseYear, genres, description }
			}}>
				<PreviewWrapper>
					<Preview src={ `http://localhost:3001/${ previewPath }` } alt="mem"/>
					<PreviewBackdrop>
						<Mark>{ usersMark } ⭐</Mark>
						<span>Release: { releaseYear }</span>
						<span>Genres: { genres }</span>
					</PreviewBackdrop>
				</PreviewWrapper>
				<FilmTitle>{ title }</FilmTitle>
			</Link>
		</FilmContainer>
	);
};

const FilmContainer = styled.li`
	margin-bottom: 50px;
	width: 200px;
	height: auto;
	max-height: 280px;
	
	a {
		display: block;
		width: inherit;
		height: inherit;
	}
`;

const PreviewWrapper = styled.div`
	margin-bottom: 10px;
	width: inherit;
	height: 280px;
	transition: 0.2s;
	
	&:hover {
		transform: scale(1.03);
	}
	
	&:hover > div {
		display: flex;
	}
`;

const Preview = styled.img`
	max-height: 280px;
	border-radius: 10px;
`;

const FilmTitle = styled.h3`
	margin: 0;
	font-size: 22px;
	white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const PreviewBackdrop = styled.div`
	box-sizing: border-box;
	position: absolute;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	display: none;
	flex-direction: column;
	justify-content: flex-end;
	padding: 10px;
	background-color: rgba(0, 0, 0, .6);
	border-radius: 10px;
	
	span {
		display: block;
		margin-bottom: 7px;
		color: #e3e3e3;
	}
	
	span:not(:first-child) {
		display: block;
		font-size: 15px;
	}
`;

const Mark = styled.span`
	font-size: 20px;
`;

export default FilmsListItem;