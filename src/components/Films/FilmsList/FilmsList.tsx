import React, { useEffect, Suspense } from 'react';
import { useParams } from "react-router";
import { useSelector, useDispatch } from "react-redux";
import styled from "styled-components";

import SectionTitle from "@/components/UI/SectionTitle/SectionTitle";
import { selectFilms, selectPending, selectPagesCount } from "@/redux/selectors/filmsSelector";
import { getFilms } from "@/redux/actions/filmActions";
import FilmsListItem from "@/components/Films/FilmsList/FilmsListItem/FilmsListItem";
import Loader from "@/components/UI/Loader/Loader";
import Pagination from "@/components/UI/Pagination/Pagination";

interface UrlParams {
	genreParam: string,
	pageParam: string
}

const FilmsList: React.FC = () => {
	const { genreParam, pageParam } = useParams<UrlParams>();
	const dispatch = useDispatch();
	const films = useSelector(selectFilms);
	const pending = useSelector(selectPending);
	const pagesCount = useSelector(selectPagesCount);

	useEffect(() => {
		if (genreParam && pageParam) dispatch(getFilms({genre: genreParam, page: pageParam}));
	}, [genreParam, pageParam, dispatch]);

	return (
		<React.Fragment>
			<SectionTitle>{ genreParam }</SectionTitle>
			{ pending ? <Loader/> : null }
			<List>
				<Suspense fallback={ <Loader/> }>
					{ films.map((filmInfo) => (
						<FilmsListItem key={ filmInfo.id }
						               genres={ filmInfo.genres }
						               mark={ filmInfo.mark }
						               releaseYear={ filmInfo.releaseYear }
						               title={ filmInfo.title }
						               previewPath={ filmInfo.previewPath }
						               id={ filmInfo.id }
						               description={ filmInfo.description }/>
					)) }
				</Suspense>
			</List>
			<Pagination basePath={ `/films/${ genreParam }/` }
			            pagesCount={ pagesCount }
			            currentPage={ +pageParam }/>
		</React.Fragment>
	);
};

const List = styled.ul`
	display: flex;
	flex-wrap: wrap;
	gap: 20px;
	margin-bottom: 30px;
`;

export default FilmsList;