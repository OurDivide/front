import React, { Dispatch, SetStateAction, useEffect } from 'react';
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { profileLinks } from "@/components/Profile/Profile";

import { selectToken } from "@/redux/selectors/authSelector";
import { selectGenres, selectLinks } from "@/redux/selectors/filmsSelector";
import NavItem from "@/components/Header/Nav/NavList/NavItem/NavItem";
import { getGenres } from "@/redux/actions/filmActions";

interface Props {
	isVisible: boolean,
	setVisible: Dispatch<SetStateAction<boolean>>
}

const NavList: React.FC<Props> = ({ isVisible, setVisible }) => {
	const dispatch = useDispatch();
	const token = useSelector(selectToken);
	const genres = useSelector(selectGenres);
	const filmsLink = useSelector(selectLinks);

	useEffect(() => {
		if (!genres.length) dispatch(getGenres());
	}, [genres, dispatch]);

	return (
		<List isVisible={ isVisible }>
			<NavItem path='/films'
			                   title='Films'
			                   subLinks={ filmsLink }
			                   setMenuVisible={ setVisible }/>
			{ !token ? <NavItem path='/auth'
			                    title='Authenticate'
			                    setMenuVisible={ setVisible }/> : null }
			{ token ? <NavItem path='/profile'
			                   title='Profile'
			                   subLinks={ profileLinks }
			                   setMenuVisible={ setVisible }/> : null }
		</List>
	);
};

const List = styled.ul<{ isVisible: boolean }>`
	box-sizing: border-box;
	position: absolute;
	top: 80px;
	right: ${ ({ isVisible }) => isVisible ? '0' : '-260px' };
	display: flex;
	flex-direction: column;
	align-items: center;
	width: 260px;
	height: calc(100vh - 80px);
	transition: right .4s;
	background-color: #fff;
	z-index: 400;
	
	@media (min-width: 768px) {
		position: static;
		flex-direction: row;
		width: auto;
		height: auto;
		transition: none;
		background-color: transparent;
	}
`;

export default NavList;