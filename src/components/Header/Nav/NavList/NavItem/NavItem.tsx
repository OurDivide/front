import React, { useState, Dispatch, SetStateAction } from 'react';
import styled from "styled-components";
import { NavLink } from "react-router-dom";
import { push } from 'connected-react-router';
import { useDispatch } from "react-redux";

import Link from "@/interfaces/NavigationLink";

interface Props {
	path: string,
	title: string,
	exact?: boolean,
	subLinks?: Link[],
	setMenuVisible: Dispatch<SetStateAction<boolean>>
}

const NavItem: React.FC<Props> = ({ path, title, exact, subLinks, setMenuVisible }) => {
	const [isSubMenuVisible, setIsSubMenuVisible] = useState(false);
	const dispatch = useDispatch();

	let subMenu: any[] = [];

	const listClickHandler = () => {
		if (subLinks) setIsSubMenuVisible(prevState => !prevState);
		else {
			setMenuVisible(false);
			dispatch(push(path));
		}
	};

	if (subLinks) {
		subLinks.forEach((link, i) => subMenu.push(
			<li key={ i }>
				<StyledNavLink to={ link.path! }
				               exact={ link.exact }
				               onClick={ () => setMenuVisible(false) } >
					{ link.title }
				</StyledNavLink>
			</li>
		))
	}

	return (
		<ListItem onClick={ listClickHandler }
		          withSubMenu={ !!subLinks }
							isSubMenuVisible={ isSubMenuVisible }>
			<StyledNavLink to={ path } exact={ exact } onClick={ () => setMenuVisible(false) }>
				{ title }
			</StyledNavLink>
			{ (isSubMenuVisible && subLinks) ? <SubNav>{ subMenu }</SubNav> : null }
		</ListItem>
	)
};

const ListItem = styled.li<{ withSubMenu: boolean, isSubMenuVisible: boolean }>`
	box-sizing: border-box;
	position: relative;
	padding: 15px;
	width: 100%;
	font-size: 24px;
	border-bottom: 1px solid #afafaf;
	cursor: pointer;
	
	&:hover {
		background-color: #f5f5f5;
	}
	
	&::after {
		content: '';
		position: absolute;
		right: 25px;
		top: ${ props => props.isSubMenuVisible ? '22px' : '15px' };
		display: block;
		width: 14px;
		height: 14px;
		border-right: ${ props => props.withSubMenu ? '1px solid #131313' : 'none' };
		border-bottom: ${ props => props.withSubMenu ? '1px solid #131313' : 'none' };
		transform: rotateZ(${ props => props.isSubMenuVisible ? '225deg' : '45deg' });
		transition: .2s ease-out;
	}
	
	@media (min-width: 768px) {
		border-bottom: none;
		
		&:hover {
			background-color: transparent;
		}
		
		&::after {
			display: none;
		}
	}
`;

const StyledNavLink = styled(NavLink).attrs({ activeClassName: 'active' })`
	position: relative;
	font-size: 24px;
	color: #131313;
	
	&.active {
		color: #61dafb;
	}
	
	&::after {
		content: '';
		position: absolute;
		bottom: -2px;
		left: 0;
		width: 0;
		height: 1px;
		border-radius: 5px;
		background-color: #61dafb;
		transition: width 0.2s ease-out;
		visibility: hidden;
	}
	
	&:hover::after {
		width: 100%;
		visibility: visible;
	}
	
	@media (min-width: 768px) {
		width: auto;
		color: #fff;
	}
`;

const SubNav = styled.ul`
	& a {
		font-size: 18px;
	}
	
	@media (min-width: 768px) {
	 display: none;
	}
`;

export default NavItem;