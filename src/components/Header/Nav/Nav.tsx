import React, { useState } from 'react';
import { NavLink } from "react-router-dom";
import styled from "styled-components";

import NavList from "@/components/Header/Nav/NavList/NavList";
import Backdrop from "@/components/UI/Backdrop/Backdrop";

export interface IsMenuVisible {
	isVisible: boolean
}

const Nav: React.FC = () => {
	const [isMenuVisible, setIsMenuVisible] = useState<boolean>(false);

	return (
		<Navigation>
			<NavLink to='/'>
				<SpanU>U</SpanU><SpanFilm>Film</SpanFilm>
			</NavLink>
			<NavList isVisible={ isMenuVisible } setVisible={ setIsMenuVisible }/>
			<Menu isVisible={ isMenuVisible } onClick={ () => setIsMenuVisible(prevState => !prevState) }>
				<HamburgerTopLine isVisible={ isMenuVisible }/>
				<HamburgerCentralLine isVisible={ isMenuVisible }/>
				<HamburgerBottomLine isVisible={ isMenuVisible }/>
			</Menu>
			{ isMenuVisible ? <Backdrop onClickHandler={ () => setIsMenuVisible(false) }/> : null }
		</Navigation>
	);
};

const Navigation = styled.nav`
	display: flex;
	justify-content: space-between;
	align-items: center;
	margin: 0 auto;
	max-width: 1024px;
	width: 90%;
`;

const SpanU = styled.span`
	color: #fb5b53;
	font-weight: 400;
	font-size: 26px;
`;

const SpanFilm = styled.span`
	color: #fff;
	font-weight: 400;
	font-size: 26px;
`;

const Menu = styled.div<IsMenuVisible>`
	position: relative;
	display: flex;
	flex-direction: column;
	justify-content: ${ ({ isVisible }) => isVisible ? 'center' : 'space-between' };
	width: 36px;
	height: 30px;
	cursor: pointer;
	z-index: 400;
	
	@media (min-width: 768px) {
		display: none;
	}
`;

const HamburgerTopLine = styled.span<IsMenuVisible>`
	position: relative;
	display: block;
	margin-top: ${ ({ isVisible }) => isVisible ? '-3px' : '0' };
	width: 36px;
	height: 4px;
	background-color: #fff;
	border-radius: 5px;
	transform-origin: center;
	transform: rotate(${ ({ isVisible }) => isVisible ? '45deg' : '0deg' });
	transition: .2s;
	
	@media (min-width: 768px) {
		display: none;
	}
`;

const HamburgerCentralLine = styled(HamburgerTopLine)`
	opacity: ${ ({ isVisible }) => isVisible ? '0' : '1' };
	transition: opacity .3s;
`;

const HamburgerBottomLine = styled(HamburgerTopLine)`
	margin-bottom: ${ ({ isVisible }) => isVisible ? '3px' : '0' };
	transform: rotate(${ ({ isVisible }) => isVisible ? '-45deg' : '0deg' });
`;

export default Nav;