import React from 'react';
import styled from "styled-components";

import Nav from "@/components/Header/Nav/Nav";

const Header: React.FC = () => {
	return (
		<HeaderTag>
			<Nav/>
		</HeaderTag>
	);
};

const HeaderTag = styled.header`
	box-sizing: border-box;
	position: fixed;
	display: flex;
	align-items: center;
	padding: 15px 25px;
	width: 100%;
	height: 80px;
	background-color: #201e1d;
	z-index: 100;
`;

export default Header;