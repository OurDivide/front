export const range = (from: number, to: number) => {
	let i = from;

	let range = [];

	while (i <= to) {
		range.push(i);
		i++;
	}

	return range;
};