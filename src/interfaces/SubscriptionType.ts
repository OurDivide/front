export default interface SubscriptionType {
	id: number,
	duration: number,
	price: number
}