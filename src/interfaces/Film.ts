export default interface Film {
	id: number,
	title: string,
	description: string,
	previewPath: string,
	genres: string,
	releaseYear: number,
	mark: number
}