export default interface Field {
	value: string,
	type: string,
	placeholder: string,
	minLength: number,
	lengthError: boolean
}