export default interface UseInputField {
	type: string,
	placeholder: string,
	minLength: number
}