export default interface Link {
	path: string,
	title: string,
	exact: boolean
}