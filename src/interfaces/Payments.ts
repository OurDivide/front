export default interface Payments {
	duration: number,
	price: number,
	date: string
}