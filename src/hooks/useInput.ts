import { useState } from 'react';

import { changeHandler } from "@/components/Auth/Auth";

const useInput = (): [string, changeHandler] => {
	const [input, setInput] = useState('');

	const onChangeHandler: changeHandler = (evt) => {
		setInput(evt.target.value);
	};

	return [input, onChangeHandler];
};

export default useInput;