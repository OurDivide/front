import { useState } from 'react';

import Field from "@/interfaces/Field";
import UseInputField from "@/interfaces/UseInputField";
import { changeHandler } from "@/components/Auth/Auth";

const useInputWithConfig = (config: UseInputField): [Field, changeHandler] => {
	const [inputObj, setInputObj] = useState<Field>({
		value: '',
		lengthError: false,
		...config
	});

	const onChangeHandler: changeHandler = (evt) => {
		setInputObj(prevState => {
			const { value } = evt.target;
			const copiedState = { ...prevState };

			copiedState.value = value;
			copiedState.lengthError = value.length < copiedState.minLength;

			return copiedState;
		});
	};

	return [inputObj, onChangeHandler];
};

export default useInputWithConfig;