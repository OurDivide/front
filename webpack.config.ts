import path from 'path';
import { Configuration, EnvironmentPlugin  } from 'webpack';
import createStyledComponentsTransformer from 'typescript-plugin-styled-components';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import { TsconfigPathsPlugin } from "tsconfig-paths-webpack-plugin";
import { config } from 'dotenv';

config();

export default {
	mode: process.env.MODE,
	devtool: 'source-map',
	entry: {
		index: './index.tsx',
	},
	output: {
		publicPath: '/',
		filename: '[name].js',
		path: path.resolve(__dirname, 'build'),
	},
	devServer: {
		contentBase: path.join(__dirname, 'build'),
		inline: true,
		hot: true,
		port: Number(process.env.PORT) || 3000,
		publicPath: '/',
		host: 'localhost',
		historyApiFallback: {
			index: '/',
		},
	},
	module: {
		rules: [
			{
				test: /\.(ts|tsx)?$/,
				loader: 'ts-loader',
				options: {
					getCustomTransformers: () => ({ before: [createStyledComponentsTransformer()] })
				}
			},
			{
				test: /\.(png|jpe?g|gif|ttf)$/i,
				use: [
					{
						loader: 'file-loader',
					},
				],
			}
		]
	},
	resolve: {
		plugins: [new TsconfigPathsPlugin()],
		extensions: ['.tsx', '.ts', '.js'],
	},
	plugins: [
		new HtmlWebpackPlugin({ template: './src/html/index.html' }),
		new CopyWebpackPlugin({ patterns: [{ from: 'assets', to: 'assets', noErrorOnMissing: true }] }),
		new EnvironmentPlugin({ ...process.env })
	]
} as Configuration;